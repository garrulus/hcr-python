import argparse
import os
import cv2
import numpy as np
from tensorflow.keras.models import load_model


def predict_image_class(model, img):
    prediction = model.predict(img)
    predicted_class_index = np.argmax(prediction, axis=1)
    return predicted_class_index


def process_image(image_path, min_area=100):
    image = cv2.imread(image_path)
    gray_img = cv2.cvtColor(cv2.medianBlur(image, 5), cv2.COLOR_BGR2GRAY)
    threshold_img = cv2.adaptiveThreshold(gray_img, 200, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 41, 25)

    kernel = np.ones((5,5), np.uint8)
    closing = cv2.morphologyEx(threshold_img, cv2.MORPH_CLOSE, kernel, iterations=2)
    contours, _ = cv2.findContours(closing, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    filtered_contours = [cnt for cnt in contours if cv2.contourArea(cnt) >= min_area]
    bounding_boxes = sorted([cv2.boundingRect(c) for c in filtered_contours], key=lambda x: x[0])

    return threshold_img, bounding_boxes


def prepare_digit(threshold_img, x, y, w, h):
    digit = threshold_img[y:y + h, x:x + w]
    resized_digit = cv2.resize(digit, (18, 18))
    padded_digit = np.pad(resized_digit, ((5, 5), (5, 5)), "constant", constant_values=0)

    return padded_digit.reshape(1, 28, 28, 1) / 255.0


def classify_digits(image_path, model, labels):
    threshold_img, bounding_boxes = process_image(image_path)



    detected_labels = []
    for i, (x, y, w, h) in enumerate(bounding_boxes):
        prepared_digit = prepare_digit(threshold_img, x, y, w, h)

        index = predict_image_class(model, prepared_digit)
        label = labels.get(index[0], "Unknown class")
        detected_labels.append(label)

    return detected_labels


def main():
    parser = argparse.ArgumentParser(description="Digit and Letter Recognition System")
    parser.add_argument('-m', '--model_path', type=str, default='best_model.h5.keras',
                        help='Path to the trained model file.')
    parser.add_argument('-i', '--image_path', type=str, default='./data/words/img.png',
                        help='Path to the image file to classify.')

    args = parser.parse_args()

    model = load_model(args.model_path)
    labels = {i: str(i) for i in range(26)}  # Assuming 0-25 for letters A-Z
    labels.update({i + 10: chr(65 + i) for i in range(26)})  # Assuming 10-35 for letters A-Z

    detected_labels = classify_digits(args.image_path, model, labels)

    print("Text:", *detected_labels)


if __name__ == "__main__":
    main()
