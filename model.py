import numpy as np
import sys

from keras.src.layers import Conv2D, MaxPooling2D, Flatten, Dropout, BatchNormalization
from tensorflow.keras.utils import to_categorical
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint


def load_az_dataset(datasetPath):
    data, labels = [], []
    try:
        for row in open(datasetPath):
            row = row.split(",")
            label = int(row[0])
            image = np.array([int(x) for x in row[1:]], dtype="uint8")
            image = image.reshape((28, 28))
            data.append(image)
            labels.append(label)
    except Exception as e:
        print(f"Failed to load data: {e}", file=sys.stderr)
        sys.exit(1)
    return (np.array(data, dtype='float32'), np.array(labels, dtype="int"))


def load_mnist_dataset():
    (trainData, trainLabels), (testData, testLabels) = mnist.load_data()
    data = np.vstack([trainData, testData])
    labels = np.hstack([trainLabels, testLabels])
    return data, labels


def load_data():
    digitsData, digitsLabels = load_mnist_dataset()
    azData, azLabels = load_az_dataset('data/az_handwritten_data.csv')
    azLabels += 10

    data = np.vstack([azData, digitsData])
    labels = np.hstack([azLabels, digitsLabels])

    return data, labels


def prepare_data(data, labels):
    data = data.reshape((len(data), -1))
    scaler = StandardScaler()

    normalized_data = scaler.fit_transform(data.astype('float32'))
    normalized_data = normalized_data.reshape((-1, 28, 28, 1))

    train_data, test_data, train_labels, test_labels = train_test_split(normalized_data, labels, test_size=0.2,
                                                                        random_state=42)
    train_data, val_data, train_labels, val_labels = train_test_split(train_data, train_labels, test_size=0.25,
                                                                      random_state=42)

    train_labels = np.argmax(to_categorical(train_labels), axis=1)
    val_labels = np.argmax(to_categorical(val_labels), axis=1)
    test_labels = np.argmax(to_categorical(test_labels), axis=1)

    return train_data, val_data, test_data, train_labels, val_labels, test_labels


def create_model():
    input_layer = Input(shape=(28, 28, 1))

    x = Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same')(input_layer)
    x = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same')(x)
    x = MaxPooling2D((2, 2))(x)

    x = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same')(x)
    x = Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same')(x)
    x = BatchNormalization()(x)
    x = MaxPooling2D((2, 2))(x)

    x = Flatten()(x)
    x = Dense(100, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dropout(0.1)(x)
    x = Dense(64, activation='relu', kernel_initializer='he_uniform')(x)
    x = Dropout(0.125)(x)
    x = BatchNormalization()(x)

    output_layer = Dense(36, activation='softmax')(x)

    return Model(inputs=input_layer, outputs=output_layer)


def train_model(model, train_data, train_labels, val_data, val_labels):
    model.compile(optimizer=SGD(learning_rate=0.01, momentum=0.9), loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    callbacks = [
        EarlyStopping(monitor='val_loss', patience=3, verbose=1),
        ModelCheckpoint(filepath='./models/best_model.h5', monitor='val_loss', save_best_only=True, verbose=1)
    ]

    model.fit(train_data, train_labels, epochs=30, verbose=1, validation_data=(val_data, val_labels),
              callbacks=callbacks)


def evaluate(model, val_data, val_labels):
    val_loss, val_acc = model.evaluate(val_data, val_labels)
    return val_loss, val_acc


def predict(model, test_data):
    predictions = model.predict(test_data)
    predicted_labels = np.argmax(predictions, axis=1)

    return predicted_labels


def main():
    data, labels = load_data()
    train_data, val_data, test_data, train_labels, val_labels, test_labels = prepare_data(data, labels)
    model = create_model()
    train_model(model, train_data, train_labels, val_data, val_labels)

    val_loss, val_acc = evaluate(model, val_data, val_labels)
    print("Validation Loss:", val_loss)
    print("Validation Accuracy:", val_acc)

    predicted_labels = predict(model, test_data)
    print(classification_report(test_labels, predicted_labels))


if __name__ == "__main__":
    main()
