# HOW to Run

This guide provides instructions on how to set up and run the project.

## Setup

- Install necessary Python packages:
  ```bash
  pip3 install -r requirements.txt
  ```
- Download az_handwritten_data.csv

## Create Model 
- run 
  ```bash
  python3 model.py
  ```


## Predict text from image
- run
  ```bash
  python3 predict.py -m $model -i $img
  ```